path: index
title: The leading Linux desktop
description: >
  A beautiful, high-quality desktop, built on the latest open source technology.
  Trusted, powerful and easy.
header_images:
  - image: public/assets/images/workstation_framework.png
  - image: public/assets/images/workstation_background.jpg
links:
  - text: Download now
    url: /workstation/download
  - text: Reviewed By TechHut
    url: https://www.youtube.com/watch?v=54aoIGKEMnk
sections:
  - sectionTitle: Why Fedora Workstation?
    content:
      - title: Reliable
        description: Each version is updated for 18 months, and upgrades between
          versions are quick and easy.
      - title: Free & private
        description: With Fedora, your desktop is your own. It's free, there are no ads,
          and your data belongs to you.
      - title: Beautiful
        description: Workstation is carefully curated to deliver a high-quality
          experience. The desktop is clean and uncluttered.
      - title: Trusted
        description: Developed in partnership with upstream projects. Rigorously tested.
          Backed by Red Hat.
      - title: Leading technology
        description: Built on the latest technologies and enhancements that open source
          has to offer.
      - title: Makes the most of your device
        description: Fedora works with hardware vendors to make excellent hardware
          support across a range of devices.
  - sectionTitle: Features for everyone
    content:
      - title: Fantastic apps
        description: Browse an open-source collection of apps, covering everything you
          might need. Includes exciting new open source apps as well as familiar
          apps from other platforms.
        image: public/assets/images/workstation_software_center.png
      - title: Workstation speaks your language
        description: Thanks to a global community of translators, Workstation is
          available in many languages.
        image: public/assets/images/workstation_languages.png
      - title: Turn the lights down low
        description: Flip a switch to turn on dark mode and give your eyes a break from
          the day. Use night light to reduce screen glare and help with sleep.
        image: public/assets/images/workstation_nightlight.png
      - title: Calendar integration
        description: Bring your online calendar to the desktop with online accounts
        image: public/assets/images/workstation_calendar.png
      - title: Helpful out of the box
        description: Fedora Workstation includes a great set of utilities, like Clocks,
          Weather and Maps.
        image: public/assets/images/workstation_outofthebox.png
  - sectionTitle: Great for developers
    content:
      - title: Virtualization made easy
        description: Boxes takes the guesswork out of using virtual machines.
        image: public/assets/images/dev1.png
      - title: Productivity-boosting desktop features
        description: Use performance mode to boost hardware speed when you need it. Turn
          off notifications with Do Not Disturb. Press the Super key and just
          type to search for what you need.
        image: public/assets/images/dev2.png
      - title: Containers ready to go
        description: Use the latest container tools from the Red Hat ecosystem. Not
          setup required. Access the Red Hat container registry.
        image: public/assets/images/dev3.png
      - title: All the tools
        description: All the tools you might need, easy to install with a single
          command. Tools supplied with high quality, by the people who make
          them.
        image: public/assets/images/dev4.png
  - sectionTitle: Get started developing with Fedora Workstation
    content:
      - description: The Fedora Developer portal has guides on developing and deploying
          applications using Fedora workstation.
        url: https://fedoradeveloper.com
        image: public/assets/images/fedora-devs.png
      - description: DNF is the Fedora package manager that enables you to install,
          update and manage OS components.
        url: https://fedoradeveloper.com
        image: public/assets/images/logo-dnf.png
      - description: Podman is a daemonless container engine for creating and managing
          containers and container images.
        url: https://fedoradeveloper.com
        image: public/assets/images/podman.svg
  - sectionTitle: Built by you
    images: public/assets/images/iot_flock_background.jpg
    sectionDescription: >-
      The Fedora Project envisions a world where everyone can benefit from free
      and open source software built by inclusive, welcoming, and open-minded
      communities.

      Fedora Workstation is created by a team in the Fedora community called the **Workstation Working Group**. It is comprised of official members who have decision making powers, as well as other contributors. [Learn more onthe Workstation Working Group Website](https://docs.fedoraproject.org/en-US/workstation-working-group/)
    content:
      - title: Report & discuss issues
        description: You can view, file, and discuss Fedora Workstation issues on [the
          Fedora Workstation issue tracker]().
      - title: Chat with the team
        description: Visit **#fedora-workstation** on [irc.libera.chat]() and on
          **#workstation** [on the Fedora Matrix Chat server]().
      - title: Join the mailing list
        description: Sign up at [desktop@lists.fedoraproject.org]() to receive meeting
          agendas and minute ([view archives]().)
      - title: Attend a meeting
        description: Open to all current and potential contributors on **Tuesdays, 10:00
          AM US Eastern** on [Bluejeans]().
