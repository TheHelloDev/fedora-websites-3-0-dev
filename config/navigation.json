{
  "downloads": {
    "label": "Get Fedora",
    "icon": "fa6-solid:download",
    "sections": [
      {
        "label": "Editions",
        "description": "Fedora Editions are the official OS editions of Fedora Linux.",
        "links": [
          {
            "label": "Workstation",
            "path": "/workstation",
            "icon": "fa6-solid:desktop",
            "description": "The flagship Fedora edition featuring the latest Gnome desktop. The Linux desktop you've been waiting for."
          },
          {
            "label": "Server",
            "path": "/server",
            "icon": "fa6-solid:server",
            "description": "Run applications on bare metal or the cloud with a Linux server OS packed with the latest open source technology."
          },
          {
            "label": "IoT",
            "path": "/iot",
            "icon": "fa6-solid:microchip",
            "description": "A foundation for Internet of Things and Device Edge ecosystem."
          },
          {
            "label": "CoreOS",
            "path": "/coreos",
            "icon": "fa6-solid:circle",
            "description": "An automatically-updating, minimal operating system for running containerized workloads securely and at scale."
          },
          {
            "label": "Cloud",
            "path": "/cloud",
            "icon": "fa6-solid:cloud",
            "description": "An automatically-updating, minimal operating system for running containerized workloads securely and at scale."
          }
        ]
      },
      {
        "label": "Emerging Editions",
        "description": "These editions are supported but not yet a part of the official Fedora editions.",
        "links": [
          {
            "label": "Silverblue",
            "path": "https://silverblue.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "Fedora Silverblue is an immutable desktop operating system aimed at good support for container-focused workflows."
          }
        ]
      },
      {
        "label": "Spins",
        "description": "The Desktop spins feature specific Linux Desktop Environments. Our official Workstation Edition uses the Gnome Desktop, but you can install Fedora preconfigured with any of the listed Desktop Environments.",
        "links": [
          {
            "label": "Fedora KDE",
            "path": "https://spins.fedoraproject.org/kde/",
            "icon": "fa6-solid:desktop",
            "description": "A complete, modern desktop built using the KDE Plasma Desktop Environment."
          },
          {
            "label": "Fedora XFCE",
            "path": "https://spins.fedoraproject.org/xfce/",
            "icon": "fa6-solid:desktop",
            "description": "A complete and well integrated desktop."
          },
          {
            "label": "Cinnamon Desktop",
            "path": "https://spins.fedoraproject.org/cinnamon/",
            "icon": "fa6-solid:desktop",
            "description": "A modern desktop featuring a traditional gnome user experience."
          },
          {
            "label": "Mate-Compiz Desktop",
            "path": "https://spins.fedoraproject.org/mate-compiz/",
            "icon": "fa6-solid:desktop",
            "description": "A classic Fedora desktop with an additional 3D window manager."
          },
          {
            "label": "i3 Tiling WM",
            "path": "https://spins.fedoraproject.org/i3/",
            "icon": "fa6-solid:desktop",
            "description": "Fedora Linux with the i3 Tiling Window Manager."
          },
          {
            "label": "LXQT Desktop",
            "path": "https://spins.fedoraproject.org/lxqt/",
            "icon": "fa6-solid:desktop",
            "description": "A lightweight and well intgrated LXQT desktop environment."
          },
          {
            "label": "LXDE Desktop",
            "path": "https://spins.fedoraproject.org/lxde/",
            "icon": "fa6-solid:desktop",
            "description": "A light, fast, and less resource hungry desktop environment"
          },
          {
            "label": "Kinoite",
            "path": "https://kinoite.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "Fedora Kinoite is an immutable KDE-based desktop."
          }
        ]
      },
      {
        "label": "Labs",
        "description": "The Labs are Fedora Linux set up with software bundles based on particular topics.",
        "links": [
          {
            "label": "Astronomy",
            "path": "https://labs.fedoraproject.org/astronomy/",
            "icon": "fa6-solid:desktop",
            "description": "Powerful, completely open-source and free tools for amateur and professional astronomers."
          },
          {
            "label": "Comp Neuro",
            "path": "https://labs.fedoraproject.org/comp-neuro/",
            "icon": "fa6-solid:desktop",
            "description": "A plethora of Free/Open source computational modelling tools for Neuroscience."
          },
          {
            "label": "Design Suite",
            "path": "https://labs.fedoraproject.org/design-suite/",
            "icon": "fa6-solid:desktop",
            "description": "Visual design, multimedia production, and publishing suite of free and open source creative tools."
          },
          {
            "label": "Games",
            "path": "https://labs.fedoraproject.org/games",
            "icon": "fa6-solid:desktop",
            "description": "A collection and perfect showcase of the best games available in Fedora."
          },
          {
            "label": "Jam",
            "path": "https://labs.fedoraproject.org/jam/",
            "icon": "fa6-solid:desktop",
            "description": "For audio enthusiasts and musicians who want to create, edit, and produce audio and music on Linux."
          },
          {
            "label": "Python Classroom",
            "path": "https://labs.fedoraproject.org/python-classroom/",
            "icon": "fa6-solid:desktop",
            "description": "A classroom lab for teaching the Python programming language to students."
          },
          {
            "label": "Robotics Suite",
            "path": "https://labs.fedoraproject.org/robotics",
            "icon": "fa6-solid:desktop",
            "description": "A wide variety of free and open robotics software packages for beginners and experts."
          },
          {
            "label": "Scientific",
            "path": "https://labs.fedoraproject.org/scientific",
            "icon": "fa6-solid:desktop",
            "description": "A bundle of open source scientific and numerical tools used in research."
          },
          {
            "label": "Security Lab",
            "path": "https://labs.fedoraproject.org/security",
            "icon": "fa6-solid:desktop",
            "description": "A bundle of open source scientific and numerical tools used in research."
          }
        ]
      }
    ]
  },
  "community": {
    "label": "Communities",
    "icon": "fa6-solid:users",
    "description": "Our community links connect you with our communication platforms, articles, blog posts, and events.",
    "sections": [
      {
        "label": "Communications",
        "description": "Our community is big and there are many ways that we connect with one another.",
        "links": [
          {
            "label": "Fedora Discussion",
            "path": "https://discussion.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "The official Fedora messaging board. Chat with other Fedorans about our many community projects."
          },
          {
            "label": "Fedora Matrix Chat",
            "path": "https://discussion.fedoraproject.org/t/how-to-link-to-a-fedora-chat-matrix-room/33186",
            "icon": "fa6-solid:desktop",
            "description": "The official chat space of Fedora Linux. This link will help you get logged into Fedora Matrix."
          },
          {
            "label": "Fedora Twitter",
            "path": "https://twitter.com/fedora",
            "icon": "fa6-solid:desktop",
            "description": "Follow us on twitter! This is a great place to stay up to date on the latest in the Fedora community."
          },
          {
            "label": "Ask Fedora",
            "path": "https://ask.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "Get help with your Fedora Operating System and learn more about the Fedora project on this official forum."
          },
          {
            "label": "Mailing Lists",
            "path": "https://lists.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "Subscribe to the mailing lists to stay up to date on Fedora's development discussions."
          },
          {
            "label": "Fedora Mastodon",
            "path": "https://mastodon.social/@fedora@fosstodon.org",
            "icon": "fa6-solid:desktop",
            "description": "Follow us on Mastodon! This is a great place to stay up to date on the latest in the Fedora community."
          }
        ]
      },
      {
        "label": "Publications",
        "description": "The Fedora Project publishes content regularly in the Community Blog and Fedora Magazine. Stay up to date with the latest in the Fedora Community.",
        "links": [
          {
            "label": "Fedora Magazine",
            "path": "https://fedoramagazine.org/",
            "icon": "fa6-solid:desktop",
            "description": "Fedora Magazine publishes user friendly F/OSS guides and articles about Fedora Linux."
          },
          {
            "label": "Community Blog",
            "path": "https://communityblog.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "The Community Blog is used to share important news and updates about things happening in the Fedora community."
          }
        ]
      },
      {
        "label": "Events",
        "description": "From our release parties to our annual conference and more, Fedorans regularly come together to celebrate our community and the hard work that we put into it.",
        "links": [
          {
            "label": "Flock To Fedora",
            "path": "/flocktofedora",
            "icon": "fa6-brands:fedora",
            "description": "The annual contributor conference for Fedora Linux. Flock takes place at the beginning of August."
          },
          {
            "label": "Week of Diversity",
            "path": "https://docs.fedoraproject.org/en-US/diversity-inclusion/",
            "icon": "fa6-solid:users",
            "description": "This is a week in September where we highlight the contributions and experiences of women, lgbtq+, and bipoc in Fedora."
          },
          {
            "label": "Release Parties",
            "path": "https://docs.fedoraproject.org/en-US/mindshare-committee/release-parties/#_what_is_a_release_party",
            "icon": "fa6-brands:fedora",
            "description": "Fedora releases two major versions every year. We celebrate the hard work of our contributors virtually at each release."
          }
        ]
      },
      {
        "label": "Extended Community",
        "description": "Beyond the official channels for Fedorans to communicate, community members are involved on other popular platforms as well!",
        "links": [
          {
            "label": "IRC",
            "path": "https://fedoraproject.org/wiki/Communicating_and_getting_help#IRC",
            "icon": "fa6-solid:message",
            "description": "Prior to the move to Matrix, Fedrans officially chatted over IRC. Many IRC spaces are still maintained by the community."
          },
          {
            "label": "Discord",
            "path": "https://discord.com/invite/fedora",
            "icon": "fa6-brands:discord",
            "description": "This is a community managed Discord server for Fedora."
          },
          {
            "label": "Telegram",
            "path": "https://fedoraproject.org/wiki/Telegram",
            "icon": "fa6-brands:telegram",
            "description": "Some SIG's in Fedora communicate over Telegram."
          },
          {
            "label": "Reddit",
            "path": "https://reddit.com/r/fedora",
            "icon": "fa6-brands:reddit",
            "description": "This is a community managed subreddit for Fedora."
          }
        ]
      }
    ]
  },
  "contributors": {
    "label": "Contributors",
    "icon": "fa6-solid:user",
    "description": "These pages connect you to all the different spaces that Fedorans work on projects. From new to long-term contributors, the following links will get you to where you want to go.",
    "sections": [
      {
        "label": "Coordination",
        "description": "General sites for getting involved and contributing to the Fedora Project.",
        "links": [
          {
            "label": "Fedora Accounts",
            "path": "#",
            "icon": "fa6-brands:fedora",
            "description": ""
          },
          {
            "label": "Mote",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Badges",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Fedocal",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Elections",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "Source Code",
        "description": "Our code can be found on the following Git Forges",
        "links": [
          {
            "label": "Gitlab",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Pagure",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Github",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "Packaging",
        "description": "Our package maintainers build their code using koji and buildah. Check out these sources and file bugs with Bugzilla.",
        "links": [
          {
            "label": "Bodhi",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Koji",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Copr",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Bugzilla",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "New Contributors",
        "description": "We are always excited to work with new contributors. If you're new to the project, check out these sites.",
        "links": [
          {
            "label": "Join SIG",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Easy Fix",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Contributor Docs",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      }
    ]
  },
  "support": {
    "label": "Support",
    "icon": "bi:life-preserver",
    "description": "An automatically-updating, minimal operating system for running containerized workloads securely and at scale.",
    "sections": [
      {
        "label": "User Support",
        "description": "These are support docs for end users.",
        "links": [
          {
            "label": "Ask Fedora",
            "path": "https://discussion.fedoraproject.org/",
            "icon": "fa6-solid:desktop",
            "description": "Ask Fedora is a message board for getting help from the community.  you need help installing, using, or customizing Fedora Linux, or have any other questions about the operating system or the Fedora Project, you’ve come to the right place!"
          },
          {
            "label": "Fedora Linux",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Workstation",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "IoT",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Server",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Cloud",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "CoreOS",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Emerging Desktops",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Spins & Labs",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "Project Docs",
        "description": "These doc pages are about the Fedora Project. They cover our organizational structure, core values, teams, legal docs, and community guidelines.",
        "links": [
          {
            "label": "About the Fedora Project",
            "path": "https://docs.fedoraproject.org/en-US/project/",
            "icon": "fa6-solid:desktop"
          },
          {
            "label": "Fedora Council",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Diversity & Inclusion",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Engineering Teams",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Mindshare Teams",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Program Management",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Packaging Guidelines",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Legal",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "Ask Fedora",
        "description": "Ask Fedora is a message board for getting help from the community.  you need help installing, using, or customizing Fedora Linux, or have any other questions about the operating system or the Fedora Project, you’ve come to the right place!",
        "links": [
          {
            "label": "New Users",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Common Issues",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Announcements",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      },
      {
        "label": "Other Sources",
        "description": "Over the years, Fedorans have created many sources of information for using our operating system. While these are not the official support sources, they are used and maintained by members of our community.",
        "links": [
          {
            "label": "Fedora Wiki",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          },
          {
            "label": "Fedora Developer",
            "path": "#",
            "icon": "fa6-solid:desktop",
            "description": ""
          }
        ]
      }
    ]
  }
}
